# +----------------------------------------------------------------------------+
# | 2Com Consulting - Infra
# |----------------------------------------------------------------------------+
# | Cliente  : 2Com
# | Tecnico  : Felipe Marques
# | Arquivo  : parametrizacao.sh
# | Objetivo : Padrao Oracle
# | Criado   : 03/07/2018
# | Alterado :
# +----------------------------------------------------------------------------+
################################################################################
#!/bin/bash

echo "** Padrao 2Com **"
echo " "
echo -n "Hostname: "
read hostname;
echo -n "IP: "
read ip;
echo " "

echo "Ajustar Hosts..."
echo "$ip	$hostname	$hostname" >> /etc/hosts
echo "...OK "
echo " "

#echo "Desabilita Firewall..."
##ervice iptables stop
#service ip6tables stop
#chkconfig iptables off
#chkconfig ip6tables off
#sed -i -e "s/SELINUX=enforcing/SELINUX=disabled/" /etc/selinux/config
setenforce 0
#echo "...OK "
#echo " "

echo "Repositorio EPEL..."
rpm -ivh http://dl.fedoraproject.org/pub/epel/6Server/x86_64/epel-release-6-8.noarch.rpm
echo "...OK "
echo " "

sleep 2

#echo "Instalar Pacotes..."
#yum -y remove samba-common
#yum -v -y install xterm tiger* twm iptraf ntpdate samba4 samba4-client openvpn xhost screen
#leep 2
#um -v -y install oracle-rdbms-server-11gR2-preinstall-1.0-13.el6.x86_64
#echo "...OK "
#cho " "

sleep 2

#echo "Criar Estrutura de Diretorios..."
#cd /
##mkdir -p /db/backup
#cd /db/backup
#mkdir checklist backup_old fisico scripts
#cd fisico
#mkdir back_db control spfile
#cd /
##mkdir /u01
#mkdir /mnt/backup
##mkdir /mnt/backup_check
#mkdir /mnt/backup_rman
#echo "...OK "
#echo " "

sleep 2

#echo "Ajustar Permissoes..."
#chown oracle:oinstall /db -R
#chown oracle:oinstall /u01 -R
#echo "...OK "
#echo " "

sleep 2

#echo "Ajustar Parametros Oracle..."
#echo " "
#echo "# Oracle Settings" >> /home/oracle/.bash_profile
#echo "TMP=/tmp; export TMP" >> /home/oracle/.bash_profile
#echo 'TMPDIR=$TMP; export TMPDIR' >> /home/oracle/.bash_profile
#echo " " >> /home/oracle/.bash_profile
#echo "ORACLE_HOSTNAME="$hostname"; export ORACLE_HOSTNAME" >> /home/oracle/.bash_profile
#echo "ORACLE_BASE=/u01/app/oracle; export ORACLE_BASE" >> /home/oracle/.bash_profile
#echo 'ORACLE_HOME=$ORACLE_BASE/product/11.2.0.4/dbhome_1; export ORACLE_HOME' >> /home/oracle/.bash_profile
#echo "ORACLE_SID=WINT; export ORACLE_SID" >> /home/oracle/.bash_profile
#echo "ORACLE_TERM=xterm; export ORACLE_TERM" >> /home/oracle/.bash_profile
#echo 'PATH=/usr/sbin:$PATH; export PATH' >> /home/oracle/.bash_profile
#echo 'PATH=$ORACLE_HOME/bin:$PATH:/u01/app/oracle/product/11.2.0.4/dbhome_1/OPatch; export PATH' >> /home/oracle/.bash_profile
#echo 'LD_LIBRARY_PATH=$ORACLE_HOME/lib:/lib:/usr/lib; export LD_LIBRARY_PATH' >> /home/oracle/.bash_profile
#echo 'CLASSPATH=$ORACLE_HOME/JRE:$ORACLE_HOME/jlib:$ORACLE_HOME/rdbms/jlib; export CLASSPATH' >> /home/oracle/.bash_profile
#echo "alias tlf='tail -f $ORACLE_BASE/diag/rdbms/ORACLE_SID/ORACLE_SID/trace/alert_$ORACLE_SID.log'" >> /home/oracle/.bash_profile
#echo " " >> /home/oracle/.bash_profile
#echo '  if [ $USER = "oracle" ]; then' >> /home/oracle/.bash_profile
##echo '    if [ $SHELL = "/bin/ksh" ]; then' >> /home/oracle/.bash_profile
#echo "      ulimit -p 16384" >> /home/oracle/.bash_profile
#echo "      ulimit -n 65536" >> /home/oracle/.bash_profile
#echo "    else" >> /home/oracle/.bash_profile
#echo "      ulimit -u 16384 -n 65536" >> /home/oracle/.bash_profile
#echo "    fi" >> /home/oracle/.bash_profile
#echo "  fi" >> /home/oracle/.bash_profile
#echo " "
#echo "Parametrizado... OK"
#echo " "